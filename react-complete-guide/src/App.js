/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import Expenses from "./components/ExpenseElements/Expenses/Expenses";
import NewExpense from "./components/ExpenseElements/NewExpense/NewExpense";

const DUMMY_EXPENSES = [
  {
    id: "e1",
    title: "Car Insurance",
    amount: 200,
    date: new Date(2021, 2, 12),
  },
  {
    id: "e2",
    title: "Car",
    amount: 20000,
    date: new Date(2021, 2, 15),
  },
  {
    id: "e3",
    title: "Car Insurance",
    amount: 200,
    date: new Date(2021, 2, 12),
  },
];

function App() {
  const [expenses, setExpenses] = useState(DUMMY_EXPENSES);

  const addExpenseHandler = (expense) => {
    setExpenses((prevState) => [expense, ...prevState]);
  };

  return (
    <div>
      <NewExpense onAddExpense={addExpenseHandler} />
      <Expenses data={expenses} />
    </div>
  );
}

export default App;
