import React, { useState } from "react";
import ExpensesFilter from "../ExpenseFilter/ExpensesFilter";
import ExpensesList from "../ExpensesList/ExpensesList";
import "./Expenses.css";
import ExpensesChart from "../ExpensesChart/ExpensesChart";
import Card from "../../UI/Card/Card";

function Expenses(props) {
  const [filter, setFilter] = useState("2021");
  const filterChangeHandler = (e) => {
    setFilter(e);
    console.log(filter);
  };

  const filteredExpenses = props.data.filter((expense) => {
    return expense.date.getFullYear().toString() === filter;
  });

  return (
    <>
      <Card className="expenses">
        <ExpensesFilter selected={filter} onValueChange={filterChangeHandler} />
        <ExpensesChart expenses={filteredExpenses} />
        <ExpensesList data={filteredExpenses} />
      </Card>
    </>
  );
}

export default Expenses;
