import React, { useState } from "react";

import ExpenseForm from "../ExpenseForm/ExpenseForm";
import "./NewExpense.css";

const NewExpense = (props) => {
  const [isEditing, setIsEditing] = useState(false);
  const saveExpenseDataHandler = (e) => {
    const expenseData = {
      ...e,
      id: Math.random().toString(),
    };
    props.onAddExpense(expenseData);
  };

  const editingHandler = () => {
    setIsEditing(!isEditing);
  };

  return (
    <div className="new-expense">
      {!isEditing ? (
        <button onClick={editingHandler}>Add New Expense</button>
      ) : (
        <ExpenseForm
          onCloseForm={editingHandler}
          onSaveExpenseData={saveExpenseDataHandler}
        />
      )}
    </div>
  );
};

export default NewExpense;
